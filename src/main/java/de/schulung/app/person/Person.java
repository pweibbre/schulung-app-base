package de.schulung.app.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Person {

  private Long id;

  private String preName;

  private String lastName;

  private int age;
}
