package de.schulung.app.person;

import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

  private Set<Person> personHashSet = new HashSet<>();

  private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);

  public PersonService() {
    // create test data
    LOGGER.info("Started creation of test data");
    Person firstPerson = Person.builder().id(1L).preName("First").lastName("Person").age(19).build();
    Person secondPerson = Person.builder().id(2L).preName("Second").lastName("Person").age(20).build();
    Person thirdPerson = Person.builder().id(3L).preName("Third").lastName("Person").age(21).build();
    createPerson(firstPerson);
    createPerson(secondPerson);
    createPerson(thirdPerson);
    LOGGER.info("Finished creation of test data");
  }

  public Collection<Person> getPersons() {
    return personHashSet;
  }

  public Person getPersonById(Long id) {
    Optional<Person> personOptional = personHashSet.stream().filter(person -> person.getId().equals(id)).findFirst();
    if(personOptional.isPresent()) {
      return personOptional.get();
    }
    return null;
  }

  public Person createPerson(Person person) {
    if(personHashSet.contains(person)) {
      LOGGER.debug("Person " + person.getPreName() + " " + person.getLastName() + " already exists");
      return null;
    }
    personHashSet.add(person);
    LOGGER.debug("Person " + person.getPreName() + " " + person.getLastName() + "with ID: " + person.getId() + " created");
    return person;
  }

  public Person deletePerson(Long id) {
    LOGGER.warn("Try to delete person with ID: " + id);
    Person personToDelete = getPersonById(id);
    if(personToDelete != null) {
      personHashSet.remove(personToDelete);
    }
    return personToDelete;
  }
}
